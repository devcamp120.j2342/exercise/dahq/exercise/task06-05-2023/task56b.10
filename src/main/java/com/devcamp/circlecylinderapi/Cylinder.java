package com.devcamp.circlecylinderapi;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder(double radius, String color) {
        super(radius, color);

    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Cylinder(double radius, String color, double height) {
        super(radius, color);
        this.height = height;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    @Override
    public String toString() {
        return "Cylinder [height=" + height + "]" + super.toString();
    }

    public double getVolume() {
        return this.height * super.getArea();
    }

}

package com.devcamp.circlecylinderapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class MainCircle {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double AreaCircle(@RequestParam(required = true, name = "radius") double radius) {
        Circle circle = new Circle(radius);

        return circle.getArea();
    }

    @GetMapping("/cylinder-volume")
    public double AreaCylinder(@RequestParam(required = true, name = "radius") double radius,
            @RequestParam(required = true, name = "height") double height) {

        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }
}
